function Rocket(){
    tRocket = new Sprite(game, "rocket.png", 150, 150);
    tRocket.setSpeed(0);
    tRocket.setAngle(90);
    tRocket.setPosition(400, 500)
    tRocket.hSpeed = 0;

    tRocket.checkKeys = function(){
        tRocket.changeImage("rocket.png")
        if(keysDown[K_LEFT]){
            this.hSpeed -= 1;
        }
        if(keysDown[K_RIGHT]){
            this.hSpeed += 1;
        }
        if(keysDown[K_UP]){
            this.addVector(0,0.5);
            tRocket.changeImage("rocketspark.png")
        }
        if (keysDown[K_DOWN]){
            this.changeSpeedBy(-1);
        }
        tRocket.changeXby(this.hSpeed)
    }

    tRocket.checkGravity = function(){
        if(this.y > 580){
            this.setPosition(this.x,580)
        }else{
            this.addVector(180,0.1);
        }
    }

    return tRocket
}


function Spark(){
    tSpark = new Sprite(game, "spark.png", 30, 30);
    tSpark.setSpeed(10);

    tSpark.wriggle = function(){
        newDir = (Math.random()*90)-45;
        this.changeAngleBy(newDir);
    }

    tSpark.reset = function(){
        //set new random position
        newX = Math.random()*this.cWidth;
        newY = Math.random()*this.cHeight;
        this.setPosition(newX,newY);
    }

    return tSpark;
}