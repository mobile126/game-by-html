function Frog() {
    tFrog = new Sprite(game, "frog.png", 50, 50);
    tFrog.minspeed = -3;
    tFrog.maxspeed = 10;
    tFrog.setSpeed(0);
    tFrog.setAngle(0);
    //method
    tFrog.checkKeys = function () {
        if (keysDown[K_RIGHT]) {
            this.changeImgAngleBy(5);
        }
        if (keysDown[K_LEFT]) {
            this.changeImgAngleBy(-5);
        }
        if (keysDown[K_UP]) {
            this.changeSpeedBy(1);
            if (this.speed > this.maxspeed) {
                tFrog.setSpeed(this.maxspeed);
            }
        }
        if (keysDown[K_DOWN]) {
            this.changeSpeedBy(-1);
            if (this.speed < this.minspeed) {
                tFrog.setSpeed(this.minspeed);
            }
        }

        tFrog.setSpeed(this.speed);
    }

    return tFrog;
}

function Fly(){
    tFly = new Sprite(game, "fly.png", 20, 30);
    tFly.setSpeed(10);

    tFly.wriggle = function(){
        newDir = (Math.random()*90)-45;
        this.changeAngleBy(newDir);
    }

    tFly.reset = function(){
        //set new random position
        newX = Math.random()*this.cWidth;
        newY = Math.random()*this.cHeight;
        this.setPosition(newX,newY);
    }

    return tFly;
}